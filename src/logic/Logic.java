package logic;

import domain.Equipo;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Logic {

    ArrayList<Equipo> lstEquipos = new ArrayList<>();

    public Logic() {
        lstEquipos.add(new Equipo("San Carlos", 0, 0, 0));
        lstEquipos.add(new Equipo("LDA", 0, 0, 0));
        lstEquipos.add(new Equipo("Saprissa", 0, 0, 0));
        lstEquipos.add(new Equipo("Heredia", 0, 0, 0));
        lstEquipos.add(new Equipo("Cartago", 0, 0, 0));
        lstEquipos.add(new Equipo("UCR", 0, 0, 0));
    }

    public void menu() {
        int entrada = 0;
        Scanner sc = new Scanner(System.in);
        while (true) {

            System.out.println("-----BIENVENIDO AL JUEGO-----");
            System.out.println(" 1.Jugar \n"
                    + " 2.Tabla Posiciones \n"
                    + " 3.Reporte \n"
                    + " 4.Salir \n");
            System.out.print("Seleccione una opcion: ");
            entrada = sc.nextInt();

            switch (entrada) {
                case 1: {
                    jugar();
                }
                default:;
            }
        }
    }

    public void jugar() {
        randomEnfrentamientos(lstEquipos);
    }

    public void randomEnfrentamientos(ArrayList<Equipo> lstE) {
        Random r = new Random();
        Object[][] enfrentamientos = new Object[3][2];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 2; j++) {
                int random = 0;
                random = r.nextInt(lstE.size());
                if (random>0) {
                    random--;
                }
                System.out.println(random+"  Largo de LstE: "+lstE.size());
                System.out.println("Posicion que tome: "+random);
                enfrentamientos[i][j]=lstE.get(random);
                lstE.remove(random);
            }
        }
        for (int i = 0; i < enfrentamientos.length; i++) {
            System.out.println("----------------------");
            for (int j = 0; j < enfrentamientos[i].length; j++) {
                Equipo e= new Equipo();
                e= (Equipo) enfrentamientos[i][j];
                System.out.println(e.getNombreEquipo());
            }
            System.out.println("----------------------------");
        }
    }

    public void reportes() {

    }

}
