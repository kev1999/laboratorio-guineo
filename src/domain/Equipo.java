
package domain;


public class Equipo {
    private String nombreEquipo;
    private int golesaFavor;
    private int golesContra;
    private int puntos;

    public Equipo() {
    }

    public Equipo(String nombreEquipo, int golesaFavor, int golesContra, int puntos) {
        this.nombreEquipo = nombreEquipo;
        this.golesaFavor = golesaFavor;
        this.golesContra = golesContra;
        this.puntos = puntos;
    }

    public String getNombreEquipo() {
        return nombreEquipo;
    }

    public int getGolesaFavor() {
        return golesaFavor;
    }

    public int getGolesContra() {
        return golesContra;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setNombreEquipo(String nombreEquipo) {
        this.nombreEquipo = nombreEquipo;
    }

    public void setGolesaFavor(int golesaFavor) {
        this.golesaFavor = golesaFavor;
    }

    public void setGolesContra(int golesContra) {
        this.golesContra = golesContra;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }
    
    
}
